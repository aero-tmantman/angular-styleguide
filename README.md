# Aerobotics Angular Styleguide

## Introduction
This guide aims to pen down regularly used techniques and styles. This ensures that 
1) The project aims to implement things in the same way throughout if possible, saving time writing it and reading the codebase.
2) Gives a platform to discuss the techniques, implement better solutions, or find more specialised solutions for more specialised cases.

## Type Checking templates

Templates don't enjoy the same level of type-checking as components, so to ensure as best as possible that the logic holds during changes, the following is suggested. Also, it gives autocompletion in the template.

### Type Aliases, Enums

When a limited number of states are possible and more can only be supported by updating the code base (i.e. it's not as simple as updating the CMS) use Type Aliases or Enums to specify it. E.g.
```javascript
export enum MAP_TYPES {
  Satellite,
  Heatmap,
  Roads
}
```

To access that in the template, the constant needs to be put on the component's viewmodek:
```javascript
// Component
export class AppComponent {
  public selectedMapType: MAP_TYPES;
  public MAP_TYPES = MAP_TYPES;
}
```
```html
// HTML Template
<div *ngIf="selectedMapType === MAP_TYPES.Satellite">
  ...
</div>
```

## Error Handling

### HTTP
This section is concerned with three aspects of the error handling: 
  1) Logging to the server, 
  2) Notification
  3) Graceful recovery.


#### Logging
The HTTP errors are handled in an interceptor, ensuring that the logging is as close to the network as possible, and not
obscured or subject to any other processing/transforms/pipes/etc before a logger catches it. Refer to the HTTP interceptor for reference.

Graceful recovery: Error handling for each failed http request is contextual - it should manage the state where it is called appropriately.
Example
```javascript
.subscribe(result => {
  this.someArray = result;
}, (error: any) => {
  if (error instanceof HttpResponseError) {
    this.someArray = [];
  }
  // If it wasn't a HTTP error but a unexpected error, rethrow the error to be handled by the ErrorHandler, otherwise it gets swallowed.
  throw error;
})
```
If you _don't_ follow this error throwing convention (like e.g. allowing errors to be swallowed) indicate why with a comment. If there isn't a comment, another developer could come across it and simply assume it was a mistake to leave it out and then put it in.
#### Notifications 
Currently the MatSnackBar is the default notification. If contextually that doesnt make sense (you'd rather just show an error message on a form, or handling upload failures better etc) use your own way to notify the user.

#### Graceful Recovery

Note on unexpected errors: Nowhere in the entire application do we try and recover from unexpected errors, and the same should go for unexpected errors generated in the pipes leading to the subscribe on HTTP requests. Only catch HTTP errors in handlers and let the other errors propagate (most likely to be handled in Angular's handleError).

I'll state the obvious, but for transient errors (that should rarely happen twice) retry the http call, and potentially take note of the reason it failed (i.e. 500 due to server timeout). If you
are working on an unreliable endpoint, this would help you detect it.

For any other error, graceful recovery is highly contextual.
There are appropriate handlers: 
403 - why was the user allowed to make the call? 
404 - what allowed the user to request a missing resource?
In the exceptional case where we need to gracefully recover, you can assign a default value to the value you were trying to
populate from the server.

## Angular Forms

### Validation 

#### Showing errors
Angular Forms has built-in functionality to set state on a form - indicated through properties like touched, invalid, and submitted.
Angular Material uses the properties set by Angular Forms to decide when to show the user error messages.
At the time of writing, invalid fields are shown when 
 1. A control is invalid **and** 
 2. either the user has interacted with (touched) the element **or** the parent form has been submitted.

If a user missed a specific input, let them know which one. Doing the following will _not_ show the user which fields they missed:
````html
<form #testForm="ngForm">
  <!-- Some content -->
  <button type="button" (click)="testForm.valid && submitTestForm()"> Submit </button>
</form>
```` 
The following convention _will_ show them by properly set the submitted property on the form:
````html
<form #testForm="ngForm" (ngSubmit)="submitTestForm(testForm)">
  <!-- Some content -->
  <button type="submit"> Submit </button>
</form>
```` 
````javascript
// Inside the related component
public submitTestForm(testForm: NgForm) {
  if (testForm.invalid) {
    return
  }
}
````
If you _disable_ a submit button while the form is invalid, you should
1) Either have a very succinct form so that the user can easily see which required fields they've missed
2) Implement your own (different to Angular Material forms) logic for when a error message on a control is shown, so that the user knows which controls are invalid.

#### Custom Validators
A custom validator should return null if there is no value in the field. If there is a value, then it should validate it.
If you require a value, use the required validator.

### Advanced Dropdowns

#### Handling single-select drop-downs with clearing option

```html
<mat-select [value]="selectedTreeId" (selectionChange)="selectTree($event.value)" [placeholder]="Select Tree">
  <mat-option *ngIf="selectedTreeId" [value]="undefined">Clear Selection</mat-option>
  <mat-option *ngFor="let tree of trees" [value]="tree.id">{{tree.name}}</mat-option>
</mat-select>
```
#### Handling multi-select drop-downs with clear all/select all
Note that Angular first applies the changes to ngModel, then calls the selectionChange function. The (click) callbacks on individual 
mat-options are called after the selectionChange function.
```html

<!-- HTML -->
<mat-select [value]="selectedTreeIds" (selectionChange)="addOrRemoveSelectedTree($event.value)" [placeholder]="Select Trees">
  <mat-option *ngIf="selectedTreeIds.length > 2" [value]="-1" class="mat-option-no-checkbox">Clear All</mat-option>
  <mat-option *ngIf="selectedTreeIds.length > 2" [value]="-2" class="mat-option-no-checkbox">Select All</mat-option>
  <mat-option *ngFor="let tree of trees" [value]="tree.id">{{tree.name}}</mat-option>
</mat-select>
```
```javascript

// Component
public selectedTreeIds(treeIds: number[]): void {
  if (treeIds.i === -1) {
    this.selectedTreeIds = []
  } else if (id === -2) {
  this.selectedTreeIds = this.trees.map(_ => _.id);
  }
  // whatever else needs to be done
}
```
```css
// SCSS
.mat-option-no-checkbox {
  ::ng-deep .mat-pseudo-checkbox {
    display: none !important;
  }
}
```

// Alternative design, having a single button for Select All/Clear All
```html
<!-- HTML -->
<mat-select multiple [(ngModel)]="selectedTreeIds">
  <mat-option #selectOrClearAllTreesOption (click)="selectOrClearAllTrees(selectOrClearAllTreesOption.selected)" [value]="-1">
    Select All/Clear All
  </mat-option>
  <mat-option *ngFor="let tree of trees" [value]="tree.id">
    {{tree.name}}
  </mat-option>
</mat-select>
```
``` javascript
// Component
public selectOrClearAllTrees(selectAll: boolean): void {
  if (selectAll) {
    this.tree = this.selectedTreeIds = tree.map(_ => _.id).push(-1);
  } else {
    this.reportingStore.selectedPestsOrDiseases = [];
  }
}
```

Potentially you can implement the constants for Select All / Clear All with enums.

## Caching

_There are only two hard things in Computer Science: cache invalidation and naming things. - Phil Karlton_

### Introduction

In your app, some data is really worth keeping at hand, because it's regularly used or referenced. Examples of this would be
user id and other user details.

If the app is small, you can load all the data either when the user opens the page or when the user logs in. As the app grows, 
the dataset can become large and either slow download time, or use a lot of user data. 

Although here we only discuss global caching and local caching, the arguments can be extrapolated to decide on which 'level'
in the application (any level between the global parent and the lowest child) caching should be done.

### Preloading

While allowing the user to log in, preload data that the user will most likely need while navigating through the application. 
You can use several strategies to determine what this data would be. Typically a very well executed rudimentary approach is 
enough, but if you have the resources and necessity for it, you can do the more advanced ways. In order or difficulty, you can 
use different ways to guess what needs to be preloaded: Best guess, Analytics data, Combining stats + User personas, etc.

### Global caching
You can have a global datastore in your application to store cached values. Since this is consumed throughout the applicaton, 
you need to ensure that these values are generally applicable.

Advantages: 

1. Simplicity

Disadvantages:

2. Be very clear what this variable holds, since components should use this if what they require is actually subtly different.

### Local caching

Advantages: 

 1. It's easier to reason about, since it's only local to the component.
 
Disadvantages: 
 
 1. Since different components in the app might want to access this same data, you might end up requesting the same data twice. 
 

## Angular internals

### Change detection (ErrorChangedAfterItHasBeenCheckedError)

Angular follows this process when  starting with a parent component and traversing down the component tree during change detection:

1) Parent updates the Input properties on the child
2) Parent calls OnChange, OnInit and DoCheck hooks on the child
3) Parent renders itself, and for each current value of a template expression stores it on the view as oldValue.
4) Parent starts the ChangeDetection run for child component
5) Parent calls the AfterViewInit hooks on the child
6) This step only happens during development. A check runs to see if .previousValue and .currentValue of each template expression on each view node is the same.

![Angular Change detection](https://aerobotics-angular-styleguide.s3.eu-central-1.amazonaws.com/change-detection-lifecycle.png)

The ErrorChangedAfterItHasBeenCheckedError error will specifically occur when the parent component has stored its current template expression value on .oldValue, then the _grandchild_ component changes that value (typically by changing a value in a stateful service, or changing its grandparent's state) during OnInit/OnChanges/DoCheck. An example is provided in [this StackBlitz](https://stackblitz.com/edit/state-initial-test-child).

Or the error happens when a component changes its state in the AfterViewInit or related hooks.

Remember that this can also happen in observables, since observables can emit values synchronously (e.g. through of(...)). 

Using setTimeout, setInterval or then to get around the above issues is usually a hack, as it implies the state values weren't set to valid values, as they were changed before the change detection cycle was even over.

Where it _does_ make sense to use ngAfterViewInit and setTimeout together, is when you need to access the DOM. The DOM view is not available when ngOnInit runs, only in ngAfterViewInit, so that is where you get the width/height, and then use setTimeout to set the height and avoid ErrorChangedAfterItHasBeenCheckedError. If any other more elegant solution exists (e.g. just CSS) rather use that.

## State Management

### Introduction

This section addresses where to store state, how to store state, and the naming conventions

### Storing state

State is either stored on a component or on service. If an application forms a hierarchy with the 'root' component at the top, then state should live as low as possible but no lower.

If state is just applicable to a component, have it on the component.

State that is shared from a parent to any of its children (any number of levels down), store it on a service. This service should contain no logic. Call the service some-service.store.ts to indicate that it is only to store state and contains no logic.

### Passing state

User @Input when
  1) You want the child component to be a "dumb" component, i.e. no dependency injection (or at least minimal dependency injection, especially in our case of using Contentful which is DI). This is the ideal situation when you have a container component fetching a list of items to display, and you build a component that will used for each item in the list.
  2) If it's just a child and parent component that needs to access the state. The complexity of putting a variable on a service just to share between a parent and child component can be removed.

Use Dependency Injection with State services when
  1) You have state that needs to be shared between parents and either several siblings under the parent, or across different 'generations' of child components under the parent component.


## Code organisation

## Order of the properties and functions
The following order is suggested:
1) public variables
2) private variables
3) constructor
4) lifecycle hookes
5) public and private functions in the order in which they are called, similar to described in Clean Code (Robert C Martin). An alternative would be have all public functions followed by all private functions, but then readability suffers. With Typescript, if "public" is used to before each function, it's easy to find all functions.

### Unit testing
Angular Components should be viewed as a encapsulated view. It's contract with the 'outside world' is basically 
  1) The Input and Output parameters
  2) The Dependency Injected services
  3) The Components imported for Dialogs and ViewChildren
  4) The public variables on the state
  5) The public functions on the state

Unit testing these components then basically consists of 
1) Setting the following inputs
  a) @Input values
  b) Dependency Injected values
2) Calling 

## User Experience

A simplified interface is easier to code, and is easier for the user to understand. If you have so much going on on a screen that 
it becomes hard to code, you most likely have so much on the screen that it's hard for the user to know what to focus on.

Don't show a user a screen and then unexpectedly populate values once a network call completes. That's lazy and very prone to causing bugs, since the values you thought were 
loaded where potentially not loaded. And you have to check that those values are loaded before e.g. the user submits the form, which isn't the forms responsibility...etc.

![Janky_loading](https://aerobotics-angular-styleguide.s3.eu-central-1.amazonaws.com/janky-loading.gif)

Rather show a loading indicator (can range from spinner to empty cards like you see on FB) globally on the component, or locally to the field.

## RXJS Recipes

TODO
